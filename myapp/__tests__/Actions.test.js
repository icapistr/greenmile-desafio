import * as types from '../src/actions/actionTypes';
import * as actions from '../src/actions/index';
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'


describe("testing actions ", () => {
    it("should change the text", () => {
        const text = "sample text";
        let expectedAction = {
            type: types.CHANGE_TEXT,
            payload: {
                text
            }
        }
        expect(actions.changeText(text)).toEqual(expectedAction)
    })
    it("should change the language", () => {
        const language = "en";
        let expectedAction = {
            type: types.CHANGE_LANGUAGE,
            payload: {
                language
            }
        }
        expect(actions.changeLanguage(language)).toEqual(expectedAction)
    })
    it("should change the module", () => {
        const module = "central";
        let expectedAction = {
            type: types.CHANGE_MODULE,
            payload: {
                module
            }
        }
        expect(actions.changeModule(module)).toEqual(expectedAction)
    })
})