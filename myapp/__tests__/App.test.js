import React from 'react';
import { shallow } from 'enzyme';
import App from '../src/components/App';

import configureStore from 'redux-mock-store'

const mockStore = configureStore();

const initialState = {
    items: [],
    loadedItems: [],
    qtdLoaded: 0,
    loadAPI: false,
    loading: false,
    text: "",
    error: "",
    language: "all",
    mod: "all",
}

const store = mockStore(initialState);


describe('testing App component', () => {
    it('should render correctly', () => {
        const wrapper = shallow(
            <App />,
            { context: { store } }
        ).dive();

    })
})
