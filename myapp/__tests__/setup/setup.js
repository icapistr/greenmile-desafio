import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import raf from './shim'

Enzyme.configure({ adapter: new Adapter() });