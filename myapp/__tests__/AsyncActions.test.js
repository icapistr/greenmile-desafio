import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../src/actions';
import * as types from '../src/actions/actionTypes';
import fetchMock from 'fetch-mock';
import expect from 'expect';


const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

const initialState = {
    items: [],
    loadedItems: [],
    qtdLoaded: 0,
    loadAPI: false,
    loading: false,
    text: "",
    error: "",
    language: "all",
    mod: "all",
}

describe('async functions', () => {
    afterEach(() => {
        fetchMock.reset()
        fetchMock.restore()
    })

    it('creates FETCH_SEARCH_SUCCESS when fetching api has been done', () => {
        const items = []
        fetchMock.getOnce('http://portal.greenmilesoftware.com/get_resources_since', items)
        const expectedActions = [
            { type: types.FETCH_SEARCH_ON_API },
            { type: types.FETCH_SEARCH_SUCCESS,  payload: { items }}
        ]
        const store = mockStore(initialState)
        return store.dispatch(actions.fetchSearch()).then(() => {
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions)
        })
    })

})