import search from '../src/reducers/searchResource'
import * as types from '../src/actions/actionTypes'

describe('reducer', () => {
    const items = ["testing"]
    it('should return the initial state', () => {
        expect(search(undefined, {})).toEqual(
            {
                items: [],
                loadedItems: [],
                qtdLoaded: 0,
                loadAPI: false,
                loading: false,
                text: "",
                error: "",
                language: "all",
                mod: "all",
            }
        )
    })

    it('should handle fetch search begin api', () => {
        const startAction = {
            type: types.FETCH_SEARCH_ON_API,
        };
        expect(search({}, startAction)).toEqual({loadAPI: true});
    })

    it('should handle fetch search begin', () => {
        const startAction = {
            type: types.FETCH_SEARCH_BEGIN,
        };
        expect(search({}, startAction)).toEqual({loading: true});
    })

    it('should handle fetch search success', () => {
        const startAction = {
            type: types.FETCH_SEARCH_SUCCESS,
            payload: {
                items
            }
        };
        expect(search({}, startAction)).toEqual({error: "", items: ["testing"], loadAPI: false, loading: false});
    })
    
    it('should handle fetch search failure', () => {
        const startAction = {
            type: types.FETCH_SEARCH_FAILURE,
            payload: {
                error: "test"
            }
        };
        expect(search({}, startAction)).toEqual({error: "test", items: [], loadAPI: false, loading: false});
    })
    
    it('should handle change language', () => {
        const startAction = {
            type: types.CHANGE_LANGUAGE,
            payload: {
                language: "en"
            }
        };
        expect(search({}, startAction)).toEqual({language: "en"});
    })

    it('should handle change module', () => {
        const startAction = {
            type: types.CHANGE_MODULE,
            payload: {
                module: "center"
            }
        };
        expect(search({}, startAction)).toEqual({mod: "center"});
    })

    it('should handle change text', () => {
        const startAction = {
            type: types.CHANGE_TEXT,
            payload: {
                text: "asd"
            }
        };
        expect(search({}, startAction)).toEqual({text: "asd"});
    })
  
})
