import {
    loadState
} from "../localStorage"

import {
    FETCH_SEARCH_BEGIN,
    FETCH_SEARCH_SUCCESS,
    FETCH_SEARCH_FAILURE,
    FETCH_SEARCH_ON_API,
    CHANGE_LANGUAGE,
    CHANGE_MODULE,
    CHANGE_TEXT,
    LOAD_NEW_SEARCH,
    LOAD_MORE_SEARCH
} from '../actions/actionTypes'

export const fetchSearchBegin = () => ({
    type: FETCH_SEARCH_BEGIN
});

const fetchSearchOnAPI = () => ({
    type: FETCH_SEARCH_ON_API
});

const fetchSearchSuccess = items => ({
    type: FETCH_SEARCH_SUCCESS,
    payload: {
        items
    }
});

const fetchSearchFailure = error => ({
    type: FETCH_SEARCH_FAILURE,
    payload: {
        error
    }
});

export const changeLanguage = language => ({
    type: CHANGE_LANGUAGE,
    payload: {
        language
    }

})

export const changeModule = module => ({
    type: CHANGE_MODULE,
    payload: {
        module
    }

})

export const changeText = text => ({
    type: CHANGE_TEXT,
    payload: {
        text
    }

})

export const loadNewSearch = () => ({
    type: LOAD_NEW_SEARCH

})

export const loadMoreSearch = (items,qtd) => ({
    type: LOAD_MORE_SEARCH,
    payload: {
        items,
        qtd
    }
})

export const fetchSearch = () => {
    return dispatch => {
        dispatch(fetchSearchOnAPI());
        return fetch("http://portal.greenmilesoftware.com/get_resources_since")
            .then(handleErrors)
            .then(res =>
                res.json()
            )
            .then(json => {
                dispatch(fetchSearchSuccess(json.splice(0, Math.floor(json.length / 8))));
                return json
            }).catch(error => dispatch(fetchSearchFailure(error)));
    };
}

export const localSearch = () => {
    return dispatch => {
        dispatch(fetchSearchBegin());
        dispatch(fetchSearchSuccess(loadState().items));
    }
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
