import { combineReducers } from 'redux';

import search from './searchResource'

const rootReducer = combineReducers({
    search,
})

export default rootReducer