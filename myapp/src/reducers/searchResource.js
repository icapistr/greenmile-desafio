import {
    FETCH_SEARCH_BEGIN,
    FETCH_SEARCH_SUCCESS,
    FETCH_SEARCH_FAILURE,
    FETCH_SEARCH_ON_API,
    CHANGE_LANGUAGE,
    CHANGE_MODULE,
    CHANGE_TEXT,
    LOAD_NEW_SEARCH,
    LOAD_MORE_SEARCH
} from '../actions/actionTypes'

const initialState = {
    items: [],
    loadedItems: [],
    qtdLoaded: 0,
    loadAPI: false,
    loading: false,
    text: "",
    error: "",
    language: "all",
    mod: "all",
}

export default function search(state = initialState, action) {
    switch (action.type) {
        case FETCH_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                loadAPI: false,
                items: action.payload.items,
                error: ""

            }
        case FETCH_SEARCH_ON_API:
            return {
                ...state,
                loadAPI: true,
            }
        case FETCH_SEARCH_FAILURE:
            return {
                ...state,
                loading: false,
                loadAPI: false,
                error: action.payload.error,
                items: []
            }

        case FETCH_SEARCH_BEGIN:
            return {
                ...state,
                loading: true,
            }
        case CHANGE_LANGUAGE:
            return {
                ...state,
                language: action.payload.language
            }
        case CHANGE_MODULE:
            return {
                ...state,
                mod: action.payload.module
            }
        case CHANGE_TEXT:
            return {
                ...state,
                text: action.payload.text
            }
        case LOAD_NEW_SEARCH:
            return {
                ...state,
                loadedItems: [],
                qtdLoaded: 0,
                loading: true
            }
        case LOAD_MORE_SEARCH:
            return {
                ...state,
                loadedItems: action.payload.items,
                qtdLoaded: action.payload.qtd,
                loading: false
            }

        default:
            return state
    }
}