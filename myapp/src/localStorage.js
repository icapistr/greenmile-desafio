export const loadState = () => {
    try {
        const serializedState = window.localStorage.getItem('items');
        if(serializedState === null) {
            return undefined
        } 
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
}

export const saveState = (state) => {
    try {
       const serializedState = JSON.stringify(state);
       window.localStorage.setItem('items', serializedState); 
    } catch (err) {
        console.log("local storage error", err)
    }
}