import React, {Component} from 'react';
import { bindActionCreators } from 'redux'
import { ContentBlock, Progressbar } from 'framework7-react';
import { connect } from 'react-redux'

import Language from './Language';
import Module from './Module';
import InputText from './InputText'
import SearchList from './SearchList'

import * as actions from '../actions/';

class Search extends Component {
    
    render() {
        
        return (
            <div className="page" data-page="home">
                <div className="page-content">
                    <InputText changeText={this.props.changeText} />
                    <div className="list-block">
                        <ul>
                            <Language changeLanguage={this.props.changeLanguage}  />
                            <Module changeModule={this.props.changeModule} />
                        </ul>
                    </div>
                    <ContentBlock>
                        { this.props.search.loadAPI ? "Loading data from API " : "" }
                        { this.props.search.error !== "" ? this.props.search.error : "" }
                        { this.props.search.loading ?  <Progressbar infinite color="blue"></Progressbar> : "" }
                    </ContentBlock>

                    <SearchList {...this.props} />
                </div>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    search: state.search
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(actions, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Search);

