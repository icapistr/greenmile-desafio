import React, {Component} from 'react';

class Header extends Component {
    render() {
        return(
            <div className="navbar">
                <a href="#" className="back link">
                <div className="navbar-inner">
                    <div className="left">
                    </div>
                    <div className="center">
                        GreenMile - Desafio
                    </div>
                    <div className="right">
                     </div>
                 </div>
                </a>
            </div>
        );
    }

}

export default Header;