import React, {Component} from 'react';

class InputText extends Component {

    render() {
        return (
            <form className="searchbar" onSubmit={(e) => e.preventDefault()} >
                <div className="searchbar-input">
                    <input type="search" ref={(e) => this.text = e} onChange={() => this.props.changeText(this.text.value)} placeholder="Search" />
                </div>
            </form>
        )
    }

}

export default InputText