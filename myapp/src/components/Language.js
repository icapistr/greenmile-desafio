import React, { Component } from 'react';

class Language extends Component {

    handleChange = () => {
        this.props.changeLanguage(this.language.value)
    }

    render() {
        return (
            <li className="">
                <a className="item-link smart-select" href="#" data-view="false" data-panel="false" data-popup="false" data-popover="false" data-picker="false" data-login-screen="false" data-sortable="false" data-page-name="false" data-template="false">
                    <div className="item-content">
                        <div className="item-inner">
                            <div className="item-title">Languages</div>
                            <select name="language" ref={(r) => this.language = r} onChange={this.handleChange} defaultValue="all">
                                <option value="all">all</option>
                                <option value="en">en</option>
                                <option value="es">es</option>
                                <option value="ja">ja</option>
                                <option value="pt">pt</option>
                                <option value="cs">cs</option>
                                <option value="hw">hw</option>
                                <option value="ru">ru</option>
                            </select>
                        </div>
                    </div>
                </a>
            </li>
        )
    }

}

export default Language