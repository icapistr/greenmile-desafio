import React, { Component } from 'react';

class Module extends Component {

    handleChange = () => {
        this.props.changeModule(this.modules.value)
    }

    render() {
        return (
            <li className="">
                <a className="item-link smart-select" href="#" data-view="false" data-panel="false" data-popup="false" data-popover="false" data-picker="false" data-login-screen="false" data-sortable="false" data-page-name="false" data-template="false">
                    <div className="item-content">
                        <div className="item-inner">
                            <div className="item-title">Modules</div>
                            <select name="module" ref={(r) => this.modules = r} onChange={this.handleChange} defaultValue="all">
                                <option value="all" >all</option>
                                <option value="JME">JME</option>
                                <option value="central">central</option>
                                <option value="LiveFeed">LiveFeed</option>
                                <option value="DEPOT">DEPOT</option>
                                <option value="BI">BI</option>
                            </select>
                        </div>
                    </div>
                </a>
            </li>
        )
    }

}

export default Module