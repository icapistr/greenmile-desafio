import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Header from './Header';
import Search from './Search';
import * as actions from '../actions/';

import {
	Framework7App, View, Pages
} from 'framework7-react';


class App extends Component {

	render() {
		return (
		<Framework7App>
			<View id="main-view" navbarThrough dynamicNavbar={true} main url="/">
				<Header />
				<Pages>
					<Search props={this.props} />
				</Pages>
			</View>
		</Framework7App>
		)
	}
}


const mapDispatchToProps = dispatch => 
	bindActionCreators(actions, dispatch)


export default connect(null, mapDispatchToProps)(App)
