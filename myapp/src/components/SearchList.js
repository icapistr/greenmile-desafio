import React, {Component} from 'react';
import {
    ContentBlock, List, ListItem, Button
} from 'framework7-react';
import { loadState } from "../localStorage"

class SearchList extends Component {

    updateDB = () => {
        this.props.fetchSearch();
    }

    loadNewItems = () => {

        this.props.loadNewSearch();
        if (loadState().items.length === 0 || loadState().items === undefined) {
            this.updateDB();
        } else {
            this.loadItems();
        }

    }

    loadItems = () => {
        let { language, mod, text, qtdLoaded, loadedItems } = this.props.search;

        if (language === "all" && mod === "all") {
            this.props.search.items.slice(qtdLoaded, qtdLoaded+15).map((e) => {
                if (e.resource.value.indexOf(text) !== -1) {
                    loadedItems.push(e);
                }
                return false
            })
        } else if (mod === "all") {
            let count = qtdLoaded;
            this.props.search.items.find((e,i) => {
                if (e.resource.language_id === language && loadedItems.length < qtdLoaded+15 && e.resource.value.indexOf(text) !== -1) {
                    if (count > 0) {
                        count--;
                    } else {
                        loadedItems.push(e);
                    }
                }
                return false
                
            })
        } else if (language === "all") {
            let count = qtdLoaded;
            this.props.search.items.find((e) => {
                if (loadedItems.length < qtdLoaded+15 && e.resource.module_id === mod && e.resource.value.indexOf(text) !== -1) {
                    if (count > 0) {
                        count--;
                    } else {
                        loadedItems.push(e);
                    }
                    
                }
                return false
            })
        } else {
            let count = qtdLoaded;
            this.props.search.items.find((e) => {
                if (loadedItems.length < qtdLoaded+15 && e.resource.module_id === mod && e.resource.language_id === language && e.resource.value.indexOf(text) !== -1) {
                    if (count > 0) {
                        count--;
                    } else {
                        loadedItems.push(e);
                    }
                }
                return false
            })
        }
        this.props.loadMoreSearch(loadedItems, qtdLoaded+15);
    }

    render() {

        let { loadedItems } = this.props.search

        return(
            <div>
                { !this.props.search.loadAPI && this.props.search.error === "" ? <Button onClick={this.loadNewItems}>Find</Button> : "" }
                <List
                    id="search-list"
                    className="searchbar-found"
                    mediaList
                >
                    {
                        loadedItems.map((e, i) => {
                            return (<ListItem mediaItem key={i} title={e.resource.value} subtitle={e.resource.module_id + ` - ` + e.resource.updated_at} />)
                        })
                    }
                </List>
                <ContentBlock>
                    {loadedItems.length > 0 ? <Button onClick={this.loadItems}>Load More</Button> : ""}
                </ContentBlock>
            </div>
        )
    }

}

export default SearchList